use into_variant::VariantFrom;

#[derive(VariantFrom)]
enum A {
    First(B),
    Second(C),
}

#[derive(VariantFrom)]
enum B {
    D(D),
}

#[derive(VariantFrom)]
enum C {
    D(D),
}

struct D {}

fn main() {}
