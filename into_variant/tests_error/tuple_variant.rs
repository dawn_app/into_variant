use into_variant::VariantFrom;

fn main() {}

#[derive(VariantFrom)]
enum MessedUp {
    Foo(Foo, Foo),
}

struct Foo {}
