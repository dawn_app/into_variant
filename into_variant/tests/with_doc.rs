use into_variant::VariantFrom;

#[derive(VariantFrom)]
enum Fill {
    /// A color
    Color(Color),
}

struct Color {}

#[test]
fn test_from_color() {
    assert!(matches!(Fill::variant_from(Color {}), Fill::Color(_)))
}
