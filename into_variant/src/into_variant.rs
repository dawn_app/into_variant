/// Similar to `Into`, converts a value into an enum variant containing the value.
///
/// Adding `#[derive(VariantFrom)]` on an enum with single-field variants will implement `IntoVariant` for all the types contained in the variants.
///
/// Example:
/// ```
/// use into_variant::{IntoVariant, VariantFrom};
///
/// #[derive(VariantFrom)]
/// enum Fill {
///     Color(Color),
///     Pattern(Pattern),
/// }
///
/// struct Color {}
/// struct Pattern {}
///
/// let variant = Color {}.into_variant();
/// assert!(matches!(variant, Fill::Color(_)))
/// ```
///
/// The `Parent` type should always be an enum – nothing else makes sense, so you definitely shouldn't implement it with e.g. a `struct` as `Parent`. Every time you do, a cat dies. In fact, you probably shouldn't implement this trait manually at all – just use `#[derive(VariantFrom)]` instead.
///
/// Technical note: the relationship between `IntoVariant` and `VariantFrom` is the opposite of the relationship between `Into` and `From`. That is, `VariantFrom<B>` is automatically implemented for types `A` where `B: IntoVariant<A>`, not the other way around.
pub trait IntoVariant<Parent>: Sized {
    #[must_use]
    fn into_variant(self) -> Parent;
}

/// Similar to `From`, creates an enum variant containing the specified value.
///
/// This is a convenience trait that simply makes use of `IntoVariant`. That is, `VariantFrom<B>` is automatically implemented for types `A` where `B: IntoVariant<A>`. You shouldn't implement this type yourself – instead, just use `#[derive(VariantFrom)]` on an enum with single-field variants.
///
/// Example:
/// ```
/// use into_variant::{IntoVariant, VariantFrom};
///
/// #[derive(VariantFrom)]
/// enum Fill {
///     Color(Color),
///     Pattern(Pattern),
/// }
///
/// struct Color {}
/// struct Pattern {}
///
/// let variant = Fill::variant_from(Color {});
/// assert!(matches!(variant, Fill::Color(_)))
/// ```
///
/// Note: This trait only makes sense on enums.
///
pub trait VariantFrom<Child>: Sized {
    #[must_use]
    fn variant_from(_: Child) -> Self;
}

/// Convenience impl: If we have `IntoVariant`, we get `VariantFrom` for free!
impl<A, B> VariantFrom<B> for A
where
    B: IntoVariant<A>,
{
    fn variant_from(x: B) -> Self {
        x.into_variant()
    }
}
