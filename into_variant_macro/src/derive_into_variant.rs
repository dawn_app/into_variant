use proc_macro2::TokenStream;
use quote::{quote, quote_spanned};

use crate::implement::generate_enum_implementations;
use crate::parse_enum::{parse_enum, InputParseError};

pub fn derive_into_variant_inner(item: TokenStream) -> TokenStream {
    let parsed_enum = parse_enum(item.clone());

    match parsed_enum {
        Ok(parsed_enum) => generate_enum_implementations(parsed_enum),
        Err(errors) => report_errors(errors),
    }
}

fn report_errors(errors: Vec<InputParseError>) -> TokenStream {
    let skip_suggestion =
        "help: you can skip a variant by annotating it with `#[into_variant(skip)]`";

    let errors = errors.into_iter().map(|error| match error {
        InputParseError::ParseError(e) => e.to_compile_error(),
        InputParseError::HasGenerics(span) => {
            quote_spanned!(span => compile_error!("into_variant does not support generics");)
        }
        InputParseError::VariantHasNamedFields(span) => {
            let error = format!("into_variant does not support named fields\n{skip_suggestion}");
            quote_spanned!(span => compile_error!(#error);)
        }
        InputParseError::VariantHasMultipleFields(span) => {
            let error = format!("into_variant requires variants to have a single field; multiple fields found\n{skip_suggestion}");
            quote_spanned!(span => compile_error!(#error);)
        }
        InputParseError::VariantHasComplexType(span) => {
            let error = format!("into_variant does not support complex types; fields must be a simple struct or enum\n{skip_suggestion}");
            quote_spanned!(span => compile_error!(#error);)
        }
        InputParseError::UnexpectedAnnotation(span) => {
            quote_spanned!(span => compile_error!("Unexpected annotation: only `into_variant(skip)` is supported");)
        }
    });

    quote! {
        #(#errors),*
    }
}
